x = 10
if x > 5:
    print("X is greater than 5")

if x >= 10:
    print("X is at least 10")
    
if x == 10:
    print("X is 10")
    
if x != 999:
    print("X is not 999")