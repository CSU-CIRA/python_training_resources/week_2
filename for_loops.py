section_separator = "*************"
for i in range(5):
    print("i:", i)

print(section_separator)

for i in range(-1, 5):
    print("i:", i)
    
print(section_separator)

for i in range(0, 10, 2):
    print("i:", i)
